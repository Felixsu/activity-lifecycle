package com.felixsu.saa;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "entering on create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initFragment();
    }

    @Override
    protected void onRestart() {
        Log.i(TAG, "entering on restart");
        super.onRestart();
    }

    @Override
    protected void onStart() {
        Log.i(TAG, "entering on start");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "entering on resume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "entering on pause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "entering on stop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "entering on destroy");
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.i(TAG, "entering on save instance state");
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.i(TAG, "entering on restore instance state");
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initView(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own actionsss", Snackbar.LENGTH_LONG)
                        .setAction("Actions", null).show();
            }
        });
    }

    private void initFragment(){
        if (getSupportFragmentManager().findFragmentByTag(MainFragment.TAG)==null){
            getSupportFragmentManager()
                    .beginTransaction().add(R.id.fragment, new MainFragment(), MainFragment.TAG).commit();
        }

        
    }
}
