package com.felixsu.saa;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment {

    public static final String TAG = MainFragment.class.getName();

    public MainFragment() {
    }

    @Override
    public void onAttach(Context context) {
        Log.i(TAG, "entering on attach");
        Log.w(TAG, "heo");
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i(TAG, "entering on create");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "entering on create view");
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(TAG, "entering on activity created");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        Log.i(TAG, "entering on view state restored");
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.i(TAG, "entering on start");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i(TAG, "entering on resume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.i(TAG, "entering on pause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.i(TAG, "entering on stop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.i(TAG, "entering on destroy view");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "entering on destroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.i(TAG, "entering on detach");
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.i(TAG, "entering on save instance state");
        super.onSaveInstanceState(outState);
    }
}
